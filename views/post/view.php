<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Записи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="post-view">
    <?php        
        Yii::$app->formatter->locale = 'ru-RU';
//        $model->created_at = Yii::$app->formatter->asDate($model->created_at);
//        $model->updated_at = Yii::$app->formatter->asDate($model->updated_at);
    ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            //'created_at',
            ['attribute' => 'created_at', 'format' => ['date', 'php:d-m-Y H:i:s']],
            //'updated_at',
            ['attribute' => 'updated_at', 'format' => ['date', 'php:d-m-Y H:i:s']],
            'content:ntext',
        ],
    ])
    ?>

</div>

